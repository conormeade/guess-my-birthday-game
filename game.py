from random import randint

name = input ("what is your name")
for guess_number in range(1,6):

    month_number = randint(1,12)
    year_number = randint(1924,2004)

    print("guess", guess_number , ": were you born in", month_number, "/", year_number, "?")

    response = input("yes or no?")

    if response == "yes":
        print("i knew it")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! lemme try again!")
